import unittest
from src.data.translator import *


class TestCases(unittest.TestCase):

    def test_preformat_0(self):
        self.assertEqual(preformat('foo,bar'), 'foo, bar')

    def test_preformat_1(self):
        self.assertEqual(preformat('foo.bar'), 'foo. bar')

    def test_preformat_2(self):
        self.assertEqual(preformat('(foo'), '( foo')

    def test_preformat_3(self):
        self.assertEqual(preformat('[foo'), ' [foo')

    def test_remove_non_whitespace_0(self):
        self.assertEqual(remove_non_space_whitespace('foo\nbar'), 'foo bar')

    def test_remove_non_whitespace_1(self):
        self.assertEqual(remove_non_space_whitespace('foo\tbar'), 'foo bar')

    def test_remove_non_whitespace_2(self):
        self.assertEqual(remove_non_space_whitespace('foo\rbar'), 'foo bar')

    def test_has_function_name_0(self):
        dsl = "remove()"
        self.assertTrue(has_function_name(dsl))

    def test_has_function_name_1(self):
        dsl = "a = b"
        self.assertFalse(has_function_name(dsl))

    def test_has_function_name_2(self):
        dsl = "remove().triml(replace(arg))"
        self.assertTrue(has_function_name(dsl))

    def test_dsl_to_atomics_01(self):
        dsl_string = "replace(arg1, arg2).toLower(arg1).replaceAll(arg3)"
        dsl_trans = [AtomicActions.action_replace,
                     AtomicActions.action_toLower,
                     AtomicActions.action_replace]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_02(self):
        dsl_string = "toLower()"
        dsl_trans = [AtomicActions.action_toLower]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_03(self):
        dsl_string = "toLower(arg)"
        dsl_trans = [AtomicActions.action_toLower]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_04(self):
        dsl_string = "toLower(arg1).replace()"
        dsl_trans = [AtomicActions.action_toLower,
                     AtomicActions.action_replace]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_05(self):
        dsl_string = "non_existing_func(arg)"
        dsl_trans = [AtomicActions.action_none]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_06(self):
        dsl_string = "replace(arg1, arg2)"
        dsl_trans = [AtomicActions.action_replace]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_07(self):
        dsl_string = "replace(non_existent_function(), arg)"
        dsl_trans = [AtomicActions.action_none]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_08(self):
        dsl_string = "trim()"
        dsl_trans = [AtomicActions.action_trim]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_09(self):
        dsl_string = "trim(left)"
        dsl_trans = [AtomicActions.action_triml]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_10(self):
        dsl_string = "trim(right)"
        dsl_trans = [AtomicActions.action_trimr]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_11(self):
        dsl_string = "obj[]"
        dsl_trans = [AtomicActions.action_elementAccess]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_12(self):
        dsl_string = "obj[].replace(arg1, arg2)"
        dsl_trans = [AtomicActions.action_elementAccess,
                     AtomicActions.action_replace]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_13(self):
        dsl_string = "remove(replace(arg1, arg2), arg3)"
        dsl_trans = [AtomicActions.action_replace, AtomicActions.action_remove]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_14(self):
        dsl_string = "replace(toUpper(arg1))"
        dsl_trans = [AtomicActions.action_toUpper,
                     AtomicActions.action_replace]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_15(self):
        dsl_string = "replace(toUpper(arg1)).trim(left).extractAll()"
        dsl_trans = [AtomicActions.action_toUpper,
                     AtomicActions.action_replace, AtomicActions.action_triml,
                     AtomicActions.action_extractAll]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_dsl_to_atomics_16(self):
        dsl_string =\
            "replace(toUpper(remove(padStringValue(side, amount, padchar))))"
        dsl_trans = [AtomicActions.action_padStringValue,
                     AtomicActions.action_remove, AtomicActions.action_toUpper,
                     AtomicActions.action_replace]
        self.assertEqual(dsl_to_atomics(dsl_string), dsl_trans)

    def test_atomics_to_dsl_01(self):
        atomics_list = []
        atomics_trans = ""
        self.assertEqual(atomics_to_dsl(atomics_list), atomics_trans)

    def test_atomics_to_dsl_02(self):
        atomics_list = [AtomicActions.action_replaceAll]
        atomics_trans = "replaceAll(@{})"
        self.assertEqual(atomics_to_dsl(atomics_list), atomics_trans)
    
    def test_atomics_to_dsl_03(self):
        atomics_list = [AtomicActions.action_extractOne,
                        AtomicActions.action_trimr,
                        AtomicActions.action_toUpper]
        atomics_trans = "extractOne(target).trim(right).toUpper()"
        self.assertEqual(atomics_to_dsl(atomics_list), atomics_trans)
    
    def test_translation_both_ways_01(self):
        dsl_string = "extractOne(target).trim(right).toUpper()"
        self.assertEqual(atomics_to_dsl(dsl_to_atomics(dsl_string)),
                         dsl_string)

    def test_translation_both_ways_02(self):
        atomics_list = [AtomicActions.action_replace,
                        AtomicActions.action_trimr,
                        AtomicActions.action_elementAccess,
                        AtomicActions.action_elementAccess]
        self.assertEqual(dsl_to_atomics(atomics_to_dsl(atomics_list)),
                         atomics_list)

    def test_elementAccess_01(self):
        self.assertEqual(dsl_to_atomics("[arg1...arg2]"),
                         [AtomicActions.action_elementAccess])

    def test_elementAccess_02(self):
        self.assertEqual(dsl_to_atomics("[...arg2]"),
                         [AtomicActions.action_elementAccess])

    def test_elementAccess_03(self):
        self.assertEqual(dsl_to_atomics("[arg1...^arg2]"),
                         [AtomicActions.action_elementAccess])

    def test_elementAccess_04(self):
        self.assertEqual(dsl_to_atomics("[...^arg2]"),
                         [AtomicActions.action_elementAccess])

    def test_string_removal_01(self):
        self.assertEqual(dsl_to_atomics("dffds = 'affsdfsd[]'"), [])

    def test_string_removal_02(self):
        self.assertEqual(dsl_to_atomics('dffds = "affsdfsd[]"'), [])


if __name__ == '__main__':
    unittest.main()
