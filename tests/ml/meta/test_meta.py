import unittest
from src.ml.met_mod.meta import initialise, unify_recs


class MyTestCase(unittest.TestCase):

    def test_initialise(self):
        result = initialise()
        self.assertTrue(result)

    def test_unify_regs(self):
        result = unify_recs([0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5])
        self.assertEqual(type(result), type(list()))
        self.assertEqual(type(result[0]), type(0))


if __name__ == '__main__':
    unittest.main()
