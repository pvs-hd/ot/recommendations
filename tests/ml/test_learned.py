import unittest
import random
from src.ml.learned import init_learned, get_learned_recs
from src.data.atomic_actions import AtomicActions


class TestCases(unittest.TestCase):

    def test_init_learned(self):
        result = init_learned(random.randint(0, 100), random.randint(0, 100))
        self.assertTrue(result)

    def test_get_learned_recs_0(self):
        init_learned(random.randint(0, 10), random.randint(0, 10))
        result = get_learned_recs([])
        self.assertEqual(result, [])

    def test_get_learned_recs_1(self):
        ma = 8
        nor = 2
        init_learned(ma, nor)
        data = []
        for i in range(16):
            try:
                data.append(AtomicActions(random.randint(0, ma-1)))
            except ValueError:
                data.append(AtomicActions(0))
        result = get_learned_recs(data)
        self.assertEqual(type(result), type(list()))
        self.assertEqual(type(result[0]), type(0))


if __name__ == '__main__':
    unittest.main()
