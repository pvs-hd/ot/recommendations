from problog.logic import Term
from problog.program import PrologString
from problog.learning import lfi
import os
import csv
import re

MODEL = """
t(0.5)::transitionModel.
t(0.5)::frequencyAnalysis.

transition_only :- transitionModel, \+frequencyAnalysis.
frequency_only :- \+transitionModel, frequencyAnalysis.
both_correct :- transitionModel, frequencyAnalysis.
neither_correct :- \+transitionModel, \+frequencyAnalysis.
"""

TERMS = []
EXAMPLES = []
LAST_TRANSMISSION = []
LAST_FREQUENCY = []

EVIDENCE_LIMIT = 200


def initialise():
    """
                    Initialises met_mod model and connected functionality

                    Keyword arguments:
                        -

                    Return Values:
                        True: Successful
                        False: An error occurred at some point

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        Only call once
    """
    global TERMS
    try:
        transition_only = Term('transition_only')
        frequency_only = Term('frequency_only')
        both_correct = Term('both_correct')
        neither_correct = Term('neither_correct')
        TERMS.append(transition_only)  # 0
        TERMS.append(frequency_only)  # 1
        TERMS.append(both_correct)  # 2
        TERMS.append(neither_correct)  # 3
        read_evidence()
        return True
    except Exception:
        return False


def write_evidence():
    """
        Writes evidence for this model to file

        Keyword arguments:
            -

        Return Values:
            -

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            Only has to be called once, but no negative effects if called more
            often
    """
    global EXAMPLES
    with open('transition_examples.csv', newline='', encoding='utf-8',
              mode='w') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quotechar='|',
                            quoting=csv.QUOTE_MINIMAL)
        for example in EXAMPLES:
            row = []
            for (term, value, cvalue) in example:
                row.append(term)
                row.append(value)
                row.append(cvalue)
                row.append('#')
            writer.writerow(row)


def read_evidence():
    """
               Reads saved evidence for this model from file

               Keyword arguments:
                    -

               Return Values:
                   -

               Side effects:
                   -

               Possible Exception:
                   -

               Restrictions:
                   Only has to be called once, but no negative effects if
                   called more often
       """
    global EXAMPLES
    evidence_loaded = []
    if os.path.isfile("./meta_examples.csv"):
        with open('meta_examples.csv', newline='', encoding='utf-8', mode='r')\
                as csvfile:
            reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in reader:
                example = ', '.join(row)
                split_example = example.split('#')
                num_examples = len(split_example) - 1
                examples_row = []
                for i in range(num_examples):
                    ex = split_example[i]
                    if ex[0] == ',':
                        ex = ex[2:]
                    ex = ex[:len(ex) - 2]
                    ex_split = ex.split(',')
                    ex_split[1] = ex_split[1][1:]
                    ex_split[2] = ex_split[2][1:]
                    term = Term(ex_split[0])
                    boolean = True
                    if ex_split[1] == 'False':
                        boolean = False
                    cvalue = int(ex_split[2])
                    new_tuple = (term, boolean, cvalue)
                    examples_row.append(new_tuple)
                evidence_loaded.append(examples_row)
        EXAMPLES = evidence_loaded


def run_model():
    """
                    Runs Meta Model

                    Keyword arguments:


                    Return Values:
                        probs: List with probabilities of Transition Model (0)
                               and Frequency Analysis (1) being correct

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        Only call once
    """
    score, weights, atoms, iteration, lfi_problem =\
        lfi.run_lfi(PrologString(MODEL), EXAMPLES)
    local_model = lfi_problem.get_model()
    probs = re.findall(r"\d\.\d+", local_model)
    prob1 = float(probs[0])
    prob2 = float(probs[1])
    probs = [prob1, prob2]
    if probs[0] == 0.0 and probs[0] == probs[1]:
        probs[0], probs[1] = 0.5, 0.5
    return probs


def make_evidence(newest_action):
    """
                        Makes new evidence for met_mod model

                        Keyword arguments:
                            newest_action: new action taken by user

                        Return Values:
                            -

                        Side effects:
                            -

                        Possible Exception:
                            -

                        Restrictions:
                            Must be called BEFORE the model is evaluated
    """
    global EXAMPLES, TERMS
    newest_int = newest_action.value
    i = 3
    if newest_int in LAST_TRANSMISSION and newest_int in LAST_FREQUENCY:
        i = 2
    elif newest_int in LAST_TRANSMISSION:
        i = 0
    elif newest_int in LAST_FREQUENCY:
        i = 1

    example = [(TERMS[i], True, 0)]
    EXAMPLES.append(example)
    if len(EXAMPLES) > EVIDENCE_LIMIT:
        EXAMPLES = EXAMPLES[1:]


def unify_recs(transmission_recs, frequency_recs):
    """
                Unifies argument lists of recs through met_mod-model

                Keyword arguments:
                    transmission_recs: List of ints that describe recs by the
                                       transmission model
                    frequency_recs: List of ints that describe recs by the
                                    transmission model

                Return Values:
                    unified_recs_unique: unified list of atomic actions
                                        provided by the models,
                                         that represent likely next step for
                                         the user

                Side effects:
                    -

                Possible Exception:
                    -

                Restrictions:
                    -
    """
    global LAST_FREQUENCY, LAST_TRANSMISSION
    num_of_recs = len(transmission_recs)
    if len(transmission_recs) != len(frequency_recs):
        print("Transmission recs: "+str(len(transmission_recs)))
        print("Frequency Recs: "+str(len(frequency_recs)))
        raise RuntimeError("Mismatched Rec list lengths")
    probs = run_model()
    shortened_probs = []
    for i in range(len(probs)):
        shortened_probs.append(int(probs[i] * 10000))

    trans_bigger = True
    try:
        multiplier = shortened_probs[0] / shortened_probs[1]
    except ZeroDivisionError:
        multiplier = 0

    if multiplier >= 1:
        trans_bigger = True
    else:
        trans_bigger = False

    unified_recs = []

    if multiplier > 10:
        multiplier = 10

    counter_trans = 0
    counter_freq = 0
    for i in range(num_of_recs * 2):
        if trans_bigger:
            unified_recs.append(transmission_recs[counter_trans])
            counter_trans += 1
            multiplier -= 2
            if multiplier <= 1:
                trans_bigger = False
        else:
            if frequency_recs[counter_freq] != -1:
                unified_recs.append(frequency_recs[counter_freq])
                counter_freq += 1
            multiplier += 2
            if multiplier >= 1:
                trans_bigger = True
        if len(set(unified_recs)) == num_of_recs:
            break
    unified_recs_unique = []
    [unified_recs_unique.append(x) for x in unified_recs if x
     not in unified_recs_unique]

    LAST_FREQUENCY = frequency_recs
    LAST_TRANSMISSION = transmission_recs

    return unified_recs_unique
